# Herald Project

Attempt to create a lightweight solution to propagate git server events to various channels.

# Current plans

Git Servers:
- Gitlab;
- Github (TODO);

Channels:
- Slack;
- Telegram (TODO);

# Architecture

![docs/architecture.png](docs/architecture.png)

# Trivia

- https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html